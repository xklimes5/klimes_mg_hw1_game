![guess-a-number](./guess-a-number.png)

## How to compile

You will need a working c++ compiler and gnu make. 

``` shell
g++ main.cc score.hpp -o game
./game
```

Score is saved to score.txt file.

