#include <cstdlib>
#include <iostream>
#include <random>
#include <ctype.h>
#include <string>
#include <ctime>
#include "score.hpp"

const int high_bound = 100;

bool is_number(const std::string& s) // from https://stackoverflow.com/questions/4654636/how-to-determine-if-a-string-is-a-number-with-c
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

int main(int argc, char *argv[]) {
	std::string input;
	std::srand(std::time(nullptr));
	int number_of_guesses = 0;
	int guess;
	int number = std::rand() % high_bound;
	std::cout << "I am thinking of number between 0 and " << high_bound << "\n";

	do {
	    std::cout << "Guess a number: ";
		std::cin >> input;
		while (!is_number(input)) {
		    std::cout << "No, that is not a number. Try again.\n";
		    std::cout << "Guess a number: ";
		    std::cin >> input;
		}
		guess = stoi(input);
		if (number == guess) {
			std::cout << "Yes! I was thinking " << guess << '\n';
		} else {
			std::cout << "No, that's not quite it, my number was " << (number > guess ? "higher" : "lower") << '\n';
		}
		number_of_guesses++;

	} while (guess != number);

	std::string name;
	std::cout << "What is your name?:";
	std::cin >> name;

	do_score(number_of_guesses, name);


	return 0;
}
